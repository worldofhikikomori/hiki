module.exports = {

    /**
     * Checks if given string is formatted as
     * a valid email address
     */
    is_an_email (email) {
        if (typeof email !== 'string') return false;
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
    },

    /**
     * Checks if given object is iterable
     *
     * @param object
     * @param withString
     * @return boolean
     * */
    iterable (object, withString = true) {
        if (object == null) { return false; }
        if (!withString && typeof object === 'string') { return false; }
        return typeof object[Symbol.iterator] === 'function';
    },

    /**
     * Adds entire object to a formData
     *
     * @param object
     * @return FormData
     */
    object_to_formdata (object) {

        let formData = new FormData();
        let key;

        for(let property in object) {
            if(object.hasOwnProperty(property)) {

                key = property;

                if(typeof object[property] === 'object' && !(object[property] instanceof File)) {

                    object_to_formdata(object[property]);

                } else {
                    formData.append(key, object[property]);
                }

            }
        }

        return formData;

    }

};